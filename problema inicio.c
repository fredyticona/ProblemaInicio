*Autor: FREDY TICONA MULLISASCA
*************************************
UNIVERSIDAD NACIONAL SAN AGUSTIN DE AREQUIPA
ESCUELA PROFESIONAL DEINGENIERIA DE TELECOMUNICACIONES
*************************************
CURSO: COMPUTACION 1
FECHA: 25 DE SEPTIEMBRE 2017
Enunciado:
Hacer un programa que convierta un numero de segundos en horas,minutos y segundos
*/
#include<iostream>

using namespace std;

int main ()
{
    //Constantes
    const int HORA = 3600;
    const int MINUTO = 60;

    //VARIABLES
    int t,h,m,s

    //Entrada
    cout<<#Tiempo en segundos: ";cin>>t;

    //Proceso
    h = t / HORA;
    t = t % HORA;
    m = t / MINUTO;
    s = t % MINUTO;

    //Salida
    cout<<"\n";
    cout<<"Hora: "<<h<<"\n";
    cout<<"Minuto: "<<m<<"\n";
    return(0);
